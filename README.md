# Java Database Library

[![Build Status Travis Main](https://img.shields.io/travis/com/ursinn/java-databaselib/main?logo=travis&label=build%20main)](https://travis-ci.com/ursinn/java-databaselib)
[![Build Status Travis Develop](https://img.shields.io/travis/com/ursinn/java-databaselib/develop?logo=travis&label=build%20develop)](https://travis-ci.com/ursinn/java-databaselib)
[![Build Status Travis Main](https://img.shields.io/jenkins/build?jobUrl=https%3A%2F%2Fci.filli-it.ch%2Fjob%2Fursinn%2Fjob%2Fjava-databaselib%2Fjob%2Fmain%2F&label=build%20main&logo=jenkins)](https://ci.filli-it.ch/job/ursinn/job/java-databaselib)
[![Build Status Travis Develop](https://img.shields.io/jenkins/build?jobUrl=https%3A%2F%2Fci.filli-it.ch%2Fjob%2Fursinn%2Fjob%2Fjava-databaselib%2Fjob%2Fdevelop%2F&label=build%20develop&logo=jenkins)](https://ci.filli-it.ch/job/ursinn/job/java-databaselib)
[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)

## SQL Databases
- MySQL / MariaDB
- PostgreSQL
- SQLite
